import * as types from './../actions/types';

const initialState = {
    products: [],
    filterProducts: [],
    response: {
        success: "",
        errors: ""
    },
    productResponse: {}
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.CREATE_PRODUCT:
            return {
                ...state,
                response: action.response,
                productResponse: action.product
            };

        case types.UPDATE_PRODUCT:
            return {
                ...state,
                response: action.response,
                productResponse: action.product
            };

        case types.DELETE_PRODUCT:
            return {
                ...state,
                response: action.response,
                productResponse: action.product
            };

        case types.FETCH_PRODUCTS:
            return {
                ...state,
                products: action.products,
            }

        case types.FILTER_PRODUCTS:
            const products = state.products.filter((product) =>
                {
                    return parseInt(product.qty) >= parseInt(action.filters.qty) &&
                    parseFloat(product.value) >= parseFloat(action.filters.value) &&
                    (
                        (action.filters.search !== "") ?
                            product.description.toUpperCase().includes(action.filters.search.toUpperCase()) ||
                            product.short_description.toUpperCase().includes(action.filters.search.toUpperCase()) ||
                            product.code.toUpperCase().includes(action.filters.search.toUpperCase()) : true
                    ) &&
                    (
                        (action.filters.withStatus) ?
                            product.status.includes(action.filters.status) : true
                    );
                }
            )
            return {
                ...state,
                filterProducts: products
            }

        case types.CLEAR_FILTERS:
            return {
                ...state,
                filterProducts: [],
            }

        default:
            return state;
    }
}