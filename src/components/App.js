import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './../store/store'
import ProductList from './product/ProductList';
import './../sass/app.scss';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlus, faTimesCircle as fasTimesCircle, faFilter } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle as farTimesCircle } from '@fortawesome/free-regular-svg-icons';
library.add( faPlus, farTimesCircle, fasTimesCircle, faFilter);

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <div className="app">
                <div className="product-list">
                    <ProductList />
                </div>
            </div>
        </Provider>
    );
  }
}

export default App;
