import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Moment from 'react-moment';
import * as actions from "../../actions/productActions";
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import ProductUpdate from './ProductUpdate';

export class ProductItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        }

        this.deleteItem  = this.deleteItem.bind(this);
        this.toggle      = this.toggle.bind(this);
    }

    componentDidUpdate(props) {
        if(this.props.response.success && props.productResponse !== this.props.productResponse) {
            this.setState({
                isOpen: false
            })
        }
    }

    deleteItem(event) {
        this.props.deleteProduct(this.props.product.id);
        event.stopPropagation();
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        const {
            code, short_description,
            status, value, qty, created_at, updated_at
        } = this.props.product;

        return (
                <tr onClick={ this.toggle }>
                    <td>{ code }</td>
                    <td>{ short_description }</td>
                    <td>{ status }</td>
                    <td>{ value }</td>
                    <td>{ qty }</td>
                    <td><Moment date={ created_at } format="DD/MM/YYYY HH:mm:ss" /></td>
                    <td><Moment date={ updated_at } format="DD/MM/YYYY HH:mm:ss" /></td>
                    <td>
                        <span onClick={ this.deleteItem }>
                            <FontAwesomeIcon size="lg" color="#e3342f" icon={['far','times-circle']} />
                        </span>
                    </td>
                    {this.state.isOpen ?
                        <Modal isOpen={ this.state.isOpen } toggle={ this.toggle } className={ this.props.className }>
                            <ModalHeader toggle={ this.toggle }>Atualizar produto</ModalHeader>
                            <ModalBody>
                                <ProductUpdate product={ this.props.product }/>
                            </ModalBody>
                        </Modal> :
                        null
                    }
                </tr>
        );
    }

}

const mapStateProps = state => ({
    productResponse: state.productReducer.productResponse,
    products: state.productReducer.products,
    response: state.productReducer.response
})

const mapActionsProps = {
    updateProduct: actions.updateProduct,
    deleteProduct: actions.deleteProduct
}

export default connect(mapStateProps, mapActionsProps)(ProductItem);