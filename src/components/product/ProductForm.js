import React, { Component } from 'react';
import { connect } from 'react-redux';
import CurrencyInput from 'react-currency-input';
import * as actions from "../../actions/productActions";

export class ProductForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            description: "",
            short_description: "",
            code: "",
            value: "",
            status: true,
            qty: "",
        };

        this.handleInputChange  = this.handleInputChange.bind(this);
        this.handleFormSubmited = this.handleFormSubmited.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        const value = (name !== "status") ? target.value : target.checked;

        console.log(target);
        console.log(name);
        console.log(value);

        this.setState({
            [name]: value
        });
    }

    handleFormSubmited(event) {
        event.preventDefault();

        const data = this.state;
        data.status = (data.status) ? "enable": "disable";
        data.value = data.value.replace(/[^0-9,]/g, '');
        data.value = data.value.replace(',', '.');

        this.props.createProduct(data);
    }

    render() {
        const {
            response
        } = this.props;

        return (
            <div className="productForm">
                <form onSubmit={ this.handleFormSubmited }>
                    <div className="group-input form-group">
                        <input className="col-sm-12" name="description" maxLength="150" onChange={ this.handleInputChange } value={ this.state.description } type="text" required/>
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-2">Descrição</label>
                    </div>

                    <div className="group-input form-group">
                        <input className="col-sm-12" name="short_description" maxLength="30" onChange={ this.handleInputChange } value={ this.state.short_description } type="text" required/>
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-6">Curta descrição</label>
                    </div>

                    <div className="group-input form-group">
                        <input className="col-sm-12" name="code" maxLength="10" onChange={ this.handleInputChange } value={ this.state.code } type="text" required/>
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-2">Código</label>
                    </div>

                    <div className="group-input form-group">
                        <CurrencyInput decimalSeparator="," prefix="R$ " thousandSeparator="." className="col-sm-12" name="value" onChangeEvent={ this.handleInputChange } value={ this.state.value } type="text" required />
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-2">Valor</label>
                    </div>

                    <div className="group-input form-group">
                        <input className="col-sm-12" name="qty" onChange={ this.handleInputChange } value={ this.state.qty } type="number" required/>
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-2">Quantidade</label>
                    </div>

                    <div className="group-input form-group">
                        <label className="col-sm-3 label-checkbox">Status</label>
                        <span className="switch">
                            <input name="status" onChange={ this.handleInputChange } checked={ this.state.status } type="checkbox" />
                            <span className="slider round"></span>
                        </span>
                    </div>

                    { (response.errors) &&
                    <div className="alert alert-danger">
                        <div>
                            { Object.values(response.errors).map( (error,i) => {
                                return <li key={i}>{error}</li>;
                            })}
                        </div>
                    </div>
                    }

                    <hr/>

                    <div className="group-input form-group">
                        <button className="btn btn-success col-sm-12" type="submit"> Enviar</button>
                    </div>
                </form>
            </div>
        );
    }

}

const mapStateProps = state => ({
    response: state.productReducer.response
})

const mapActionsProps = {
    createProduct: actions.createProduct,
}

export default connect(mapStateProps, mapActionsProps)(ProductForm);