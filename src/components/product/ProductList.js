import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';
import $ from 'jquery';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as actions from './../../actions/productActions'
import { Modal, ModalHeader, ModalBody, Collapse } from 'reactstrap';
import ProductItem from './ProductItem';
import ProductForm from './ProductForm';
import ProductFilters from './ProductFilters';

export class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            collapse: false
        }

        this.loadProducts();
        this.toggle        = this.toggle.bind(this);
        this.collapseClick = this.collapseClick.bind(this);
        this.onEntering    = this.onEntering.bind(this);
        this.onExiting     = this.onExiting.bind(this);
    }

    componentDidUpdate(props) {
        if(this.props.response.success && props.productResponse !== this.props.productResponse) {
            this.setState({
                isOpen: false
            })

            Swal.fire({
                title: 'Sucesso!',
                text: this.props.response.success,
                type: 'success'
            })

            this.loadProducts();
        }
    }

    loadProducts() {
        this.props.fetchProducts();
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    collapseClick() {
        this.setState({
            collapse: !this.state.collapse
        })
    }

    onEntering() {
        $('.btn-menu-filter').css({
            "border-top-left-radius": "0px",
            "border-bottom-left-radius": "0px"
        });
        $('.menu-filter').removeClass('.animated rotateOutUpRight');
        $('.menu-filter').addClass('animated rotateInDownRight');
    }

    onExiting() {
        $('.btn-menu-filter').css({
            "border-top-left-radius": "35px",
            "border-bottom-left-radius": "35px"
        });
        $('.menu-filter').removeClass('.animated rotateInDownRight');
        $('.menu-filter').addClass('animated rotateOutUpRight');
        $('.btn-menu-filter').css({
            "border-top-left-radius": "35px",
            "border-bottom-left-radius": "35px"
        });
    }

    render() {
        const {
            products,
            filterProducts
        } = this.props;

        return (
            <div>
                <h4>Lista de produtos</h4>
                <hr/>
                <table className="table table-hover table-responsive-sm table-product">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Código</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Status</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Qtd.</th>
                            <th scope="col">Criado em</th>
                            <th scope="col">Atualizado em</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>

                    <tbody>
                    { (filterProducts.length > 0) ?
                        filterProducts.map( (product,i) => (
                            <ProductItem key={i} product={ product }/>
                        ))
                        :
                        products.map( (product,i) => (
                            <ProductItem key={i} product={ product }/>
                        ))
                    }
                    </tbody>
                </table>
                
                <button className="btn btn-warning btn-circle btn-menu-filter" onClick={ this.collapseClick }>
                    <FontAwesomeIcon size="1x" color="white" icon="filter"/>
                </button>

                <Collapse isOpen={ this.state.collapse } onEntering={ this.onEntering } onExiting={ this.onExiting } className="menu-filter">
                    <ProductFilters />
                </Collapse>

                <button onClick={ this.toggle } className="btn btn-primary btn-circle add-product">
                    <FontAwesomeIcon size="1x" color="white" icon="plus"/>
                </button>

                {this.state.isOpen ?
                    <Modal isOpen={ this.state.isOpen } toggle={ this.toggle } className={ this.props.className }>
                        <ModalHeader toggle={ this.toggle }>Adicionar produto</ModalHeader>
                        <ModalBody>
                            <ProductForm />
                        </ModalBody>
                    </Modal> :
                    null
                }
            </div>
        );
    }

}

const mapStateProps = state => ({
    productResponse: state.productReducer.productResponse,
    products: state.productReducer.products,
    filterProducts: state.productReducer.filterProducts,
    response: state.productReducer.response
})

const mapActionsProps = {
    fetchProducts: actions.fetchProducts,
}

export default connect(mapStateProps, mapActionsProps)(ProductList);