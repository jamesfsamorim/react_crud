import React, { Component } from 'react';
import { connect } from 'react-redux';
import CurrencyInput from 'react-currency-input';
import * as actions from "../../actions/productActions";

export class ProductFilters extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: "",
            value: "0,00",
            withStatus: false,
            status: true,
            qty: 1,
        };

        this.handleInputChange  = this.handleInputChange.bind(this);
        this.handleFormSubmited = this.handleFormSubmited.bind(this);
        this.clear              = this.clear.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        const value = (name !== "status") && (name !== "withStatus") ? target.value : target.checked;

        this.setState({
            [name]: value
        });
    }

    handleFormSubmited(event) {
        event.preventDefault();

        const data = this.state;
        data.status = (data.status) ? "enable" : "disable";
        data.value = data.value.replace(/[^0-9,]/g, '');
        data.value = data.value.replace(',', '.');

        this.props.filterProducts(data);
        this.state.value = "0,00";
    }

    clear() {
        this.props.clearFilters();
    }

    render() {
        return (
            <div className="productFilters">
                <form onSubmit={ this.handleFormSubmited }>
                    <div className="group-input form-group">
                        <input className="col-sm-12" name="search" maxLength="150" onChange={ this.handleInputChange } value={ this.state.search } type="text" />
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-12">Código ou descrições</label>
                    </div>

                    <div className="group-input form-group">
                        <CurrencyInput decimalSeparator="," prefix="R$ " thousandSeparator="." className="col-sm-12" name="value" onChangeEvent={ this.handleInputChange } value={ this.state.value } type="text"  />
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-2">Valor</label>
                    </div>

                    <div className="group-input form-group">
                        <input className="col-sm-12" name="qty" onChange={ this.handleInputChange } value={ this.state.qty } type="number" />
                        <span className="highlight"></span>
                        <span className="bar col-sm-12"></span>
                        <label className="col-sm-2">Quantidade</label>
                    </div>

                    <div className="group-input form-group">
                        <label className="col-sm-6 label-checkbox">Com Status ?</label>
                        <span className="switch">
                            <input name="withStatus" onChange={ this.handleInputChange } checked={ this.state.withStatus } type="checkbox" />
                            <span className="slider round"></span>
                        </span>
                    </div>

                    { this.state.withStatus ?
                        <div className="group-input form-group">
                                <label className="col-sm-6 label-checkbox">Status</label>
                                <span className="switch">
                                <input name="status" onChange={ this.handleInputChange } checked={ this.state.status } type="checkbox" />
                                <span className="slider round"></span>
                            </span>
                        </div> : null
                    }
                    <hr/>

                    <div className="group-input form-group">
                        <span className="btn btn-warning btn-clear-filter col-sm-12" onClick={ this.clear }> Clear</span>
                    </div>

                    <div className="group-input form-group">
                        <button className="btn btn-success col-sm-12" type="submit"> Enviar</button>
                    </div>
                </form>
            </div>
        );
    }

}

const mapStateProps = state => ({
    response: state.productReducer.response
})

const mapActionsProps = {
    filterProducts: actions.filterProducts,
    clearFilters: actions.clearFilters
}

export default connect(mapStateProps, mapActionsProps)(ProductFilters);