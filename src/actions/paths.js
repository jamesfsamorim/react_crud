export const BASE_URL                  = "http://18.228.14.48/";
export const API_PRODUCTS              = BASE_URL + "api/products";
export const FETCH_PRODUCTS_URL        = API_PRODUCTS + "?cmd=list";
export const FETCH_DETAIL_PRODUCT_URL  = API_PRODUCTS + "?cmd=details&id=";