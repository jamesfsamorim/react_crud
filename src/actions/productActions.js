import * as paths from './paths';
import * as types from './types'

export const fetchProducts = () => (dispatch) => {
    fetch(paths.FETCH_PRODUCTS_URL, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'content-type': 'application/json',
        }
    })
        .then(res =>
            res.json()
        )
        .then( response => {
            const products = response;

            dispatch({
                type: types.FETCH_PRODUCTS,
                products: products
            })
        })
}

export const deleteProduct = (id) => (dispatch) => {
    fetch(paths.API_PRODUCTS + "/" + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'content-type': 'application/json',
        }
    })
        .then(res =>
            res.json()
        )
        .then( response => {
            let res  = "";
            let product = "";

            if(response.errors) {
                res = response;
                product = {};
            }
            else {
                res  = {"success": "Produto deletado com sucesso !"};
                product = response;
            }

            dispatch({
                type: types.DELETE_PRODUCT,
                response: res,
                product: product
            })
        })
}

export const updateProduct = (data) => (dispatch) => {
    fetch(paths.API_PRODUCTS + "/" + data.id, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'content-type': 'application/json',
        },
        body: JSON.stringify(data)
    })
        .then(res =>
            res.json()
        )
        .then( response => {
            let res  = "";
            let product = "";

            if(response.errors) {
                res = response;
                product = {};
            }
            else {
                res  = {"success": "Produto atualizado com sucesso !"};
                product = response;
            }

            dispatch({
                type: types.UPDATE_PRODUCT,
                response: res,
                product: product
            })
        })
}

export const createProduct = (data) => (dispatch) => {
    fetch(paths.API_PRODUCTS, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'content-type': 'application/json',
        },
        body: JSON.stringify(data)
    })
        .then(res =>
            res.json()
        )
        .then( response => {
            let res  = "";
            let product = "";

            if(response.errors) {
                res = response;
                product = {};
            }
            else {
                res  = {"success": "Produto criado com sucesso !"};
                product = response;
            }


            dispatch({
                type: types.CREATE_PRODUCT,
                response: res,
                product: product
            })
        })
}

export const filterProducts = (filters) => (dispatch) => {
    dispatch({
        type: types.FILTER_PRODUCTS,
        filters: filters
    })
}

export const clearFilters = () => (dispatch) => {
    dispatch({
        type: types.CLEAR_FILTERS
    })
}