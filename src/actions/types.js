//-------------* PRODUCT ACTIONS *-------------//
export const CREATE_PRODUCT   = "CREATE_PRODUCT";
export const DELETE_PRODUCT   = "DELETE_PRODUCT";
export const UPDATE_PRODUCT   = "UPDATE_PRODUCT";
export const FETCH_PRODUCTS   = "FETCH_PRODUCTS";

//-------------* FILTER ACTIONS *--------------//
export const FILTER_PRODUCTS  = "FILTER_PRODUCTS";
export const CLEAR_FILTERS    = "CLEAR_FILTERS";